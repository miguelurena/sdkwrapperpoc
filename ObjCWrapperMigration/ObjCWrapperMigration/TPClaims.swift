//
//  TPClaims.swift
//  Sample
//

import SwiftJWT
import Foundation

@objcMembers class TPClaims: NSObject, Claims {
    let iss: String
    var iat: Date // no later than 60mins from now
    let payload: Payload
    
    init(iss: String, iat: Date, payload: Payload) {
        self.iss = iss
        self.iat = iat
        self.payload = payload
    }

    func update(iat: Date) {
        self.iat = iat
    }
}

@objcMembers class TPResponseClaims: NSObject, Claims {
    let iat: Date
    let payload: ResponsePayload
    
    init(iat: Date, payload: ResponsePayload) {
        self.iat = iat
        self.payload = payload
    }
}

@objcMembers class ResponsePayload: NSObject, Claims {
    let requestReference: String
    let version: String
    let response: [ResponseObject]
    let jwt: String?
    
    init(requestReference: String, version: String, response: [ResponseObject], jwt: String?) {
        self.requestReference = requestReference
        self.version = version
        self.response = response
        self.jwt = jwt
    }
}

@objcMembers class ResponseObject: NSObject, Claims {
    let requestTypedescription: String
    let errorCode: String
    let xid: String?
    let eci: String?
    let cavv: String?
    let enrolled: String?
    let threeDVersion: String?
    
    init(requestTypedescription: String, errorCode: String, xid: String?, eci: String?, cavv: String?, enrolled: String?, threeDVersion: String?) {
        self.requestTypedescription = requestTypedescription
        self.errorCode = errorCode
        self.xid = xid
        self.eci = eci
        self.cavv = cavv
        self.enrolled = enrolled
        self.threeDVersion = threeDVersion
    }
}

@objcMembers class Payload: NSObject, Codable {
    let requesttypedescriptions: [String]
    let threedbypasspaymenttypes: [String]?
    let termurl: String?
    let locale: String?
    let accounttypedescription: String
    let sitereference: String
    let currencyiso3a: String?
    let baseamount: Int?
    let mainamount: Double?
    let pan: String?
    let expirydate: String?
    let securitycode: String?
    let parenttransactionreference: String?
    let cachetoken: String?
    let subscriptiontype: String?
    let subscriptionfinalnumber: String?
    let subscriptionunit: String?
    let subscriptionfrequency: String?
    let subscriptionnumber: String?
    let credentialsonfile: String?
    let threedresponse: String?
    let paymenttypedescription: String?
    let orderreference: String?
    let applicationtype: String?
    // billing data
    let billingprefixname: String?
    let billingfirstname: String?
    let billingmiddlename: String?
    let billinglastname: String?
    let billingsuffixname: String?
    let billingstreet: String?
    let billingtown: String?
    let billingcounty: String?
    let billingcountryiso2a: String?
    let billingpostcode: String?
    let billingemail: String?
    let billingtelephone: String?
    let billingpremise: String?
    // delivery data
    let customerprefixname: String?
    let customerfirstname: String?
    let customermiddlename: String?
    let customerlastname: String?
    let customersuffixname: String?
    let customerstreet: String?
    let customertown: String?
    let customercounty: String?
    let customercountryiso2a: String?
    let customerpostcode: String?
    let customeremail: String?
    let customertelephone: String?
    let customerpremise: String?

    @objc init(requestTypeDescriptions: [String],
         threeDBypassPaymentTypes: [String]? = nil,
         termUrl: String? = "https://payments.securetrading.net/process/payments/mobilesdklistener",
         locale: String? = nil,
         accountTypeDescription: String,
         siteReference: String,
         currencyIso3a: String? = nil,
         baseAmount: Int,
         mainAmount: Double,
         pan: String? = nil,
         expiryDate: String? = nil,
         securityCode: String? = nil,
         parentTransactionReference: String? = nil,
         cacheToken: String? = nil,
         subscriptionType: String? = nil,
         subscriptionFinalNumber: String? = nil,
         subscriptionUnit: String? = nil,
         subscriptionFrequency: String? = nil,
         subscriptionNumber: String? = nil,
         credentialsOnFile: String? = nil,
         threeDResponse: String? = nil,
         paymentTypeDescription: String? = nil,
         orderReference: String? = nil,
         applicationType: String? = nil,
         billingPrefixName: String? = nil,
         billingFirstName: String? = nil,
         billingMiddleName: String? = nil,
         billingLastName: String? = nil,
         billingSuffixName: String? = nil,
         billingStreet: String? = nil,
         billingTown: String? = nil,
         billingCounty: String? = nil,
         billingCountryIso2a: String? = nil,
         billingPostcode: String? = nil,
         billingEmail: String? = nil,
         billingTelephone: String? = nil,
         billingPremise: String? = nil,
         customerPrefixName: String? = nil,
         customerFirstName: String? = nil,
         customerMiddleName: String? = nil,
         customerLastName: String? = nil,
         customerSuffixName: String? = nil,
         customerStreet: String? = nil,
         customerTown: String? = nil,
         customerCounty: String? = nil,
         customerCountryIso2a: String? = nil,
         customerPostcode: String? = nil,
         customerEmail: String? = nil,
         customerTelephone: String? = nil,
         customerPremise: String? = nil) {
        self.requesttypedescriptions = requestTypeDescriptions
        self.threedbypasspaymenttypes = threeDBypassPaymentTypes
        self.termurl = termUrl
        self.locale = locale
        self.accounttypedescription = accountTypeDescription
        self.sitereference = siteReference
        self.currencyiso3a = currencyIso3a
        self.baseamount = baseAmount
        self.mainamount = nil
        self.pan = pan
        self.expirydate = expiryDate
        self.securitycode = securityCode
        self.parenttransactionreference = parentTransactionReference
        self.cachetoken = cacheToken
        self.subscriptiontype = subscriptionType
        self.subscriptionfinalnumber = subscriptionFinalNumber
        self.subscriptionunit = subscriptionUnit
        self.subscriptionfrequency = subscriptionFrequency
        self.subscriptionnumber = subscriptionNumber
        self.credentialsonfile = credentialsOnFile
        self.threedresponse = threeDResponse
        self.paymenttypedescription = paymentTypeDescription
        self.orderreference = orderReference
        self.applicationtype = applicationType
        self.billingprefixname = billingPrefixName
        self.billingfirstname = billingFirstName
        self.billingmiddlename = billingMiddleName
        self.billinglastname = billingLastName
        self.billingsuffixname = billingSuffixName
        self.billingstreet = billingStreet
        self.billingtown = billingTown
        self.billingcounty = billingCounty
        self.billingcountryiso2a = billingCountryIso2a
        self.billingpostcode = billingPostcode
        self.billingemail = billingEmail
        self.billingtelephone = billingTelephone
        self.billingpremise = billingPremise
        self.customerprefixname = customerPrefixName
        self.customerfirstname = customerFirstName
        self.customermiddlename = customerMiddleName
        self.customerlastname = customerLastName
        self.customersuffixname = customerSuffixName
        self.customerstreet = customerStreet
        self.customertown = customerTown
        self.customercounty = customerCounty
        self.customercountryiso2a = customerCountryIso2a
        self.customerpostcode = customerPostcode
        self.customeremail = customerEmail
        self.customertelephone = customerTelephone
        self.customerpremise = customerPremise
    }
}

@objcMembers class PersonalDetails: NSObject, Codable {
    let prefixName: String?
    let firstName: String?
    let middleName: String?
    let lastName: String?
    let suffixName: String?
    let street: String?
    let town: String?
    let county: String?
    let countryIso2a: String?
    let postcode: String?
    let email: String?
    let telephone: String?
    let premise: String?
    
    @objc init(prefixName: String? = nil,
         firstName: String? = nil,
         middleName: String? = nil,
         lastName: String? = nil,
         suffixName: String? = nil,
         street: String? = nil,
         town: String? = nil,
         county: String? = nil,
         countryIso2a: String? = nil,
         postcode: String? = nil,
         email: String? = nil,
         telephone: String? = nil,
         premise: String? = nil) {
        self.prefixName = prefixName
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.suffixName = suffixName
        self.street = street
        self.town = town
        self.county = county
        self.countryIso2a = countryIso2a
        self.postcode = postcode
        self.email = email
        self.telephone = telephone
        self.premise = premise
    }
}

extension String {
    private var payload: Payload? {
        let body = components(separatedBy: ".")[1]
        guard let decoded = body.base64UrlDecode() else {
            return nil
        }
        guard let payload = try? JSONDecoder().decode(TPClaims.self, from: decoded).payload else {
            return nil
        }
        return payload
    }

    var parentReference: String? {
        payload?.parenttransactionreference
    }

    var requestTypeDescriptions: [String]? {
        payload?.requesttypedescriptions
    }
}

extension String {
    func base64UrlDecode() -> Data? {
        var base64 = self
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        let length = Double(base64.lengthOfBytes(using: String.Encoding.utf8))
        let requiredLength = 4 * ceil(length / 4.0)
        let paddingLength = requiredLength - length
        if paddingLength > 0 {
            let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
            base64 += padding
        }
        return Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
    }
}
