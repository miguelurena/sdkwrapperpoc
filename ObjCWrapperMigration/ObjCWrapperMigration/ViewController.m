//
//  ViewController.m
//  ObjCWrapperMigration
//

#import "ViewController.h"
#import <TrustPayments/TrustPayments-Swift.h>
#import <TrustPayments/TrustPayments-umbrella.h>
#import "ObjCWrapperMigration-Swift.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    Payload *payload = [[Payload alloc] initWithRequestTypeDescriptions:@[@"THREEDQUERY", @"AUTH"] threeDBypassPaymentTypes:NULL termUrl:@"https://payments.securetrading.net/process/payments/mobilesdklistener" locale:NULL accountTypeDescription:@"ECOM" siteReference:@"test_pgsmobilesdk79458" currencyIso3a:@"GBP" baseAmount:399 mainAmount:299 pan:NULL expiryDate:NULL securityCode:NULL parentTransactionReference:NULL cacheToken:NULL subscriptionType:NULL subscriptionFinalNumber:NULL subscriptionUnit:NULL subscriptionFrequency:NULL subscriptionNumber:NULL credentialsOnFile:NULL threeDResponse:NULL paymentTypeDescription:NULL orderReference:NULL applicationType:NULL billingPrefixName:NULL billingFirstName:NULL billingMiddleName:NULL billingLastName:NULL billingSuffixName:NULL billingStreet:NULL billingTown:NULL billingCounty:NULL billingCountryIso2a:NULL billingPostcode:NULL billingEmail:NULL billingTelephone:NULL billingPremise:NULL customerPrefixName:NULL customerFirstName:NULL customerMiddleName:NULL customerLastName:NULL customerSuffixName:NULL customerStreet:NULL customerTown:NULL customerCounty:NULL customerCountryIso2a:NULL customerPostcode:NULL customerEmail:NULL customerTelephone:NULL customerPremise:NULL];
    
    TPClaims *claim = [[TPClaims alloc] initWithIss:@"jwt-pgsmobilesdk"
                                                iat:[[NSDate alloc] initWithTimeIntervalSinceNow:0] payload:payload];
    
    NSString *jwt = [JWTUtil getDecodedClaimWithTpClaim:claim];
    
    UIViewController *vc = [[[ViewControllerFactory shared] dropInViewControllerWithJwt:jwt customDropInView:NULL visibleFields:@[@0,@1,@2] applePayConfiguration:NULL apmsConfiguration:NULL dropInViewStyleManager:NULL dropInViewDarkModeStyleManager:NULL cardinalStyleManager:NULL cardinalDarkModeStyleManager:NULL error:NULL payButtonTappedClosureBeforeTransaction: NULL transactionResponseClosure:^(NSArray<NSString *> * jwt, TPAdditionalTransactionResult * result, NSError * error) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Success"
                                    message:@""
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }] viewController];
    
    [[self navigationController] pushViewController:vc animated:TRUE];
}


@end
