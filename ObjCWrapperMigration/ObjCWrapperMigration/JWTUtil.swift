//
//  JWTUtil.swift
//  ObjCWrapperMigration
//

import UIKit
import Foundation

@objcMembers class JWTUtil: NSObject {
    static func getDecodedClaim(tpClaim: TPClaims) -> String {
        guard let jsonData = tpClaim.toJSONString().data(using: .utf8),
              let claim = try? JSONDecoder().decode(TPClaims.self, from: jsonData) else {
            // Handling case where payload can't be converted into a claim
            return ""
        }
        
        let jwt = JWTHelper.createJWT(basedOn: claim, signWith: "2-a4b5bc22c4fe6563c44de99034a87a092b45784f85b7941db68f2317e4e0a266")
        return jwt ?? ""
    }
    
}

extension Encodable {
    func toJSONString() -> String {
        guard let jsonData = try? JSONEncoder().encode(self) else {
            return "no json data"
        }
        return String(data: jsonData, encoding: .utf8) ?? "no encoded json data"
    }
}

func instantiate<T: Decodable>(jsonString: String) -> T? {
    return try? JSONDecoder().decode(T.self, from: jsonString.data(using: .utf8)!)
}
