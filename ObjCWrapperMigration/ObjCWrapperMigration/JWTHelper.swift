//
//  JWTHelper.swift
//  Sample
//

import SwiftJWT
import Foundation

/// Helper class for creating and signing JSON Web Tokens
/// used for communication with TP Gateway
@objcMembers class JWTHelper {
    static func createJWT<T: Claims>(basedOn claim: T, signWith secret: String) -> String? {
        // <header>.<payload>.<signature>
        // default header
        let header = Header(typ: "JWT")

        // base JWT without signature
        var jwt = JWT(header: header, claims: claim)
        guard let keyData = secret.data(using: .utf8) else {
            print("Missing secret data")
            return nil
        }
        let jwtSigner = JWTSigner.hs256(key: keyData)

        // sign jwt with key
        guard let signedJWT = try? jwt.sign(using: jwtSigner) else {
            print("Error while signing JWT: Missing secret key")
            return nil
        }

        // verify JWT
        let jwtVerifier = JWTVerifier.hs256(key: keyData)
        guard JWT<T>.verify(signedJWT, using: jwtVerifier) else {
            print("Could not verify signed token")
            return nil
        }

        return signedJWT
    }

    static func verifyJwt(jwt: String, secret: String) -> Bool {
        guard let keyData = secret.data(using: .utf8) else {
            print("Missing secret data")
            return false
        }

        let jwtVerifier = JWTVerifier.hs256(key: keyData)
        guard JWT<TPResponseClaims>.verify(jwt, using: jwtVerifier) else {
            print("Could not verify signed token")
            return false
        }

        return true
    }

    static func getTPResponseClaims(jwt: String, secret: String) throws -> TPResponseClaims? {
        guard let keyData = secret.data(using: .utf8) else {
            print("Missing secret data")
            return nil
        }

        let jwtVerifier = JWTVerifier.hs256(key: keyData)

        let jwtDecoder = JWTDecoder(jwtVerifier: jwtVerifier)
        let tpResponseClaims = try jwtDecoder.decode(JWT<TPResponseClaims>.self, fromString: jwt).claims
        return tpResponseClaims
    }

    static func getTPClaims(jwt: String, secret: String) throws -> TPClaims? {
        guard let keyData = secret.data(using: .utf8) else {
            print("Missing secret data")
            return nil
        }

        let jwtVerifier = JWTVerifier.hs256(key: keyData)
        let jwtDecoder = JWTDecoder(jwtVerifier: jwtVerifier)
        let tpClaims = try jwtDecoder.decode(JWT<TPClaims>.self, fromString: jwt).claims
        return tpClaims
    }
    
    static func decodeJWTtoString(jwt: String) -> String? {
        // Split the JWT into its constituent parts (Header, Payload, Signature)
        let segments = jwt.components(separatedBy: ".")
        
        // Ensure that the JWT has at least two segments (Header and Payload)
        // We are only interested in the Payload for decoding
        guard segments.count >= 2 else {
            return nil
        }

        // Extract the Payload part of the JWT
        // Try to deserialize the JSON data into a JSON object
        // Then try to print it back into a readable string
        guard let base64String = segments[1].base64UrlDecode(),
              let jsonObject = try? JSONSerialization.jsonObject(with: base64String, options: []),
              let jsonReadablePrintedData = try? JSONSerialization.data(withJSONObject: jsonObject, options: [.prettyPrinted, .sortedKeys]),
              let readablePrintedString = String(data: jsonReadablePrintedData, encoding: .utf8) else {
                return nil
        }

        // Return the pretty-printed JSON string
        return readablePrintedString
    }
}
