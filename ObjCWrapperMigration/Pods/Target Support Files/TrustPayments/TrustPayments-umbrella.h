#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "TrustPayments3DSecure.h"
#import "TrustPaymentsCard.h"
#import "TrustPaymentsCore.h"
#import "TrustPaymentsUI.h"

FOUNDATION_EXPORT double TrustPaymentsVersionNumber;
FOUNDATION_EXPORT const unsigned char TrustPaymentsVersionString[];

